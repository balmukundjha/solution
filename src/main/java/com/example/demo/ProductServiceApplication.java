package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

// Spring boot main class(ENTRY POINT)

@ComponentScan("com.example.demo")
@SpringBootApplication
public class ProductServiceApplication {

   public static void main(String[] args) {
      SpringApplication.run(ProductServiceApplication.class, args);
   }

}

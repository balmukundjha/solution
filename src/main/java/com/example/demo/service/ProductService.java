package com.example.demo.service;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Product;
import com.example.demo.repository.ProductRepository;

//Service class
@Service
public class ProductService {
	
	//Autowiring Repository class(Dependency Injection)
	@Autowired
	private ProductRepository productRepository;
	

	//Method to fetch all the product Details from H2 Database
	public List<Product> getProducts() {
		List<Product> products = new ArrayList<>();

		productRepository.findAll().forEach(products::add);

		return products;
	}
	//Method to fetch product Details from H2 Database with particular ID
	public Optional<Product> getProductById(int id) {
		return productRepository.findById((long) id);

	}
	//Method to fetch product Details from H2 Database with particular Color
	public Iterable<Product> getProductByColor(Iterable<Long> color) {
		return productRepository.findAllById(color);
	}
	//Method to fetch product Details from H2 Database with particular Name
	public Iterable<Product> getProductByName(Iterable<Long> name) {
		return productRepository.findAllById(name);
	}

}

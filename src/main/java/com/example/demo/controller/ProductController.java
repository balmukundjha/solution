package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Product;
import com.example.demo.service.ProductService;

//COntroller Class

//Controller Class with RestFul web service feature
@RestController
public class ProductController {
	
	//Auto wiring Service Class(Dependency Injection)
	@Autowired
	ProductService productService;

	//Fetching all the products from h2 Database
	@RequestMapping(method = RequestMethod.GET, value = "/Product/allProducts")
	@ResponseBody
	public List<Product> getProducts() {
		return productService.getProducts();
	}

	// TESTING SAMPLE METHOD to print HELLO on the Screen
	@RequestMapping(method = RequestMethod.GET, value = "/Product/sampleText")
	@ResponseBody
	public String getProducts1() {
		return "Hello";
	}

	//Fetching product with particular id
	@GetMapping("/products/{id}")
	public Optional<Product> getProductById(@PathVariable(required = true) int id) {
		return productService.getProductById(id);
	}

	//Fetching product with particular color
	@GetMapping("/products/{color}")
	public Iterable<Product> getProductByColor(@PathVariable(required = true) Iterable<Long> color) {
		return productService.getProductByColor(color);
	}

	//Fetching product with particular Name
	@GetMapping("/products/{name}")
	public Iterable<Product> getProductByName(@PathVariable(required = true) Iterable<Long> name) {
		return productService.getProductByName(name);
	}
}
